# An Explainable 3D Residual Self-Attention Deep Neural Network FOR Joint Atrophy Localization and Alzheimer's Disease Diagnosis using Structural MRI [Link](https://ieeexplore.ieee.org/document/9381587)

By Xin Zhang, Liangxiu Han, Wenyong Zhu, Liang Sun, Daoqiang Zhang

Cite as:
X. Zhang, L. Han, W. Zhu, L. Sun and D. Zhang, "An Explainable 3D Residual Self-Attention Deep Neural Network For Joint Atrophy Localization and Alzheimer's Disease Diagnosis using Structural MRI," in IEEE Journal of Biomedical and Health Informatics, 2021, doi: 10.1109/JBHI.2021.3066832.

## Introduction

In this paper, we have proposed a novel computer-aided approach for early diagnosis of AD by introducing an explainable 3D Residual Attention Deep Neural Network (3D ResAttNet) for end-to-end learning from sMRI scans. 

In this repository we release models from the paper. 

![Flowchat](./imgs/figure1.png)





