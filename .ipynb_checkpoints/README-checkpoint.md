# An Explainable 3D Residual Self-Attention Deep Neural Network FOR Joint Atrophy Localization and Alzheimer's Disease Diagnosis using Structural MRI

By Xin Zhang, Liangxiu Han, Wenyong Zhu, Liang Sun, Daoqiang Zhang

## Introduction

In this paper, we have proposed a novel computer-aided approach for early diagnosis of AD by introducing an explainable 3D Residual Attention Deep Neural Network (3D ResAttNet) for end-to-end learning from sMRI scans. 

In this repository we release models from the paper. We will release our code available after the final acceptance of the paper.

![Flowchat](./imgs/figure1.png)




